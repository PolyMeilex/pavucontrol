# translation of pavucontrol.master-tx.po to Simplified Chinese
# 甘露 (Gan Lu) <rhythm.gan@gmail.com>, 2008.msgid "".
# Leah Liu <lliu@redhat.com>, 2009.
# Simplified Chinese Version of pavucontrol.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 玉堂白鹤 <yjwork@qq.com>, 2021.
# Lv Genggeng <lvgenggeng@uniontech.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: pavucontrol.master-tx\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-15 18:55+0300\n"
"PO-Revision-Date: 2021-12-04 09:16+0000\n"
"Last-Translator: Lv Genggeng <lvgenggeng@uniontech.com>\n"
"Language-Team: Chinese (Simplified) <https://translate.fedoraproject.org/"
"projects/pulseaudio/pavucontrol/zh_CN/>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.9.1\n"

#: ../src/pavucontrol.desktop.in.h:1 ../src/pavucontrol.cc:874
msgid "PulseAudio Volume Control"
msgstr "PulseAudio 音量控制"

#: ../src/pavucontrol.desktop.in.h:2 ../src/pavucontrol.glade.h:33
msgid "Volume Control"
msgstr "音量控制"

#: ../src/pavucontrol.desktop.in.h:3
msgid "Adjust the volume level"
msgstr "调节音量水平"

#: ../src/pavucontrol.desktop.in.h:4
msgid ""
"pavucontrol;Microphone;Volume;Fade;Balance;Headset;Speakers;Headphones;Audio;"
"Mixer;Output;Input;Devices;Playback;Recording;System Sounds;Sound Card;"
"Settings;Preferences;"
msgstr "pavucontrol；麦克风体积褪色均衡头戴式耳机；发言者；耳机；音频搅拌机；输出输入装置；回放；记录；系统声音；声卡；设置；偏好;"

#: ../src/pavucontrol.glade.h:1
msgid "<b>left-front</b>"
msgstr "<b>左-前</b>"

#: ../src/pavucontrol.glade.h:3
#, no-c-format
msgid "<small>50%</small>"
msgstr "<small>50%</small>"

#: ../src/pavucontrol.glade.h:4
msgid "Card Name"
msgstr "声卡名称"

#: ../src/pavucontrol.glade.h:5
msgid "Lock card to this profile"
msgstr "锁定声卡到这个配置文件"

#: ../src/pavucontrol.glade.h:6
msgid "<b>Profile:</b>"
msgstr "<b>配置：</b>"

#: ../src/pavucontrol.glade.h:7
msgid "<b>Codec:</b>"
msgstr "<b>编码器：</b>"

#: ../src/pavucontrol.glade.h:8
msgid "Device Title"
msgstr "设备标题"

#: ../src/pavucontrol.glade.h:9
msgid "Mute audio"
msgstr "音频静音"

#: ../src/pavucontrol.glade.h:10
msgid "Lock channels together"
msgstr "锁定声道"

#: ../src/pavucontrol.glade.h:11
msgid "Set as fallback"
msgstr "设置为默认"

#: ../src/pavucontrol.glade.h:12
msgid "<b>Port:</b>"
msgstr "<b>端口：</b>"

#: ../src/pavucontrol.glade.h:13
msgid "PCM"
msgstr "PCM"

#: ../src/pavucontrol.glade.h:14
msgid "AC-3"
msgstr "AC-3"

#: ../src/pavucontrol.glade.h:15
msgid "DTS"
msgstr "DTS"

#: ../src/pavucontrol.glade.h:16
msgid "E-AC-3"
msgstr "E-AC-3"

#: ../src/pavucontrol.glade.h:17
msgid "MPEG"
msgstr "MPEG"

#: ../src/pavucontrol.glade.h:18
msgid "AAC"
msgstr "AAC"

#: ../src/pavucontrol.glade.h:19
msgid "<b>Latency offset:</b>"
msgstr "<b>延迟偏移：</b>"

#: ../src/pavucontrol.glade.h:20
msgid "ms"
msgstr "毫秒"

#: ../src/pavucontrol.glade.h:21
msgid "Advanced"
msgstr "高级"

#: ../src/pavucontrol.glade.h:22
msgid "All Streams"
msgstr "所有流"

#: ../src/pavucontrol.glade.h:23
msgid "Applications"
msgstr "应用程序"

#: ../src/pavucontrol.glade.h:24
msgid "Virtual Streams"
msgstr "虚拟流"

#: ../src/pavucontrol.glade.h:25
msgid "All Output Devices"
msgstr "所有输出设备"

#: ../src/pavucontrol.glade.h:26
msgid "Hardware Output Devices"
msgstr "硬件输出设备"

#: ../src/pavucontrol.glade.h:27
msgid "Virtual Output Devices"
msgstr "虚拟输出设备"

#: ../src/pavucontrol.glade.h:28
msgid "All Input Devices"
msgstr "所有输入设备"

#: ../src/pavucontrol.glade.h:29
msgid "All Except Monitors"
msgstr "所有设备，除了监视器"

#: ../src/pavucontrol.glade.h:30
msgid "Hardware Input Devices"
msgstr "硬件输入设备"

#: ../src/pavucontrol.glade.h:31
msgid "Virtual Input Devices"
msgstr "虚拟输入设备"

#: ../src/pavucontrol.glade.h:32
msgid "Monitors"
msgstr "监视器"

#: ../src/pavucontrol.glade.h:34
msgid "<i>No application is currently playing audio.</i>"
msgstr "<i>没有程序正在播放音频。</i>"

#: ../src/pavucontrol.glade.h:35
msgid "<b>_Show:</b>"
msgstr "<b>显示(_S)：</b>"

#: ../src/pavucontrol.glade.h:36
msgid "_Playback"
msgstr "回放(_P)"

#: ../src/pavucontrol.glade.h:37
msgid "<i>No application is currently recording audio.</i>"
msgstr "<i>没有程序正在录音。</i>"

#: ../src/pavucontrol.glade.h:38
msgid "_Recording"
msgstr "录音(_R)"

#: ../src/pavucontrol.glade.h:39
msgid "<i>No output devices available</i>"
msgstr "<i>没有可用的输出设备</i>"

#: ../src/pavucontrol.glade.h:40
msgid "<b>S_how:</b>"
msgstr "<b>显示(_h)：</b>"

#: ../src/pavucontrol.glade.h:41
msgid "_Output Devices"
msgstr "输出设备(_O)"

#: ../src/pavucontrol.glade.h:42
msgid "<i>No input devices available</i>"
msgstr "<i>没有可用的输入设备</i>"

#: ../src/pavucontrol.glade.h:43
msgid "<b>Sho_w:</b>"
msgstr "<b>显示(_w)：</b>"

#: ../src/pavucontrol.glade.h:44
msgid "_Input Devices"
msgstr "输入设备(_I)"

#: ../src/pavucontrol.glade.h:45
msgid "<i>No cards available for configuration</i>"
msgstr "<i>没有可用来配置的声卡</i>"

#: ../src/pavucontrol.glade.h:46
msgid "Show volume meters"
msgstr "显示音量表"

#: ../src/pavucontrol.glade.h:47
msgid "_Configuration"
msgstr "配置（_C）"

#: ../src/pavucontrol.glade.h:48
msgid "<b>Rename device to:</b>"
msgstr "<b>重命名设备为：</b>"

#: ../src/pavucontrol.glade.h:49
msgid "Stream Title"
msgstr "流媒体标题"

#: ../src/pavucontrol.glade.h:50
msgid "direction"
msgstr "方向"

#: ../src/pavucontrol.cc:104
#, c-format
msgid "could not read JSON from list-codecs message response: %s"
msgstr "无法从列表编解码器消息响应中读取JSON： %s"

#: ../src/pavucontrol.cc:113
msgid "list-codecs message response is not a JSON array"
msgstr "列表编解码器消息响应不是JSON数组"

#: ../src/pavucontrol.cc:161
msgid "list-codecs message response could not be parsed correctly"
msgstr "无法正确分析列表编解码器消息响应"

#: ../src/pavucontrol.cc:181
#, c-format
msgid "could not read JSON from get-codec message response: %s"
msgstr "无法从接收编码器消息响应中读取JSON：%s"

#: ../src/pavucontrol.cc:190
msgid "get-codec message response is not a JSON value"
msgstr "接受编码器消息响应不是JSON值"

#: ../src/pavucontrol.cc:198
msgid "could not get codec name from get-codec message response"
msgstr "不能从接收编码器消息响应中拿到编码器的名称"

#: ../src/pavucontrol.cc:220
#, c-format
msgid "could not read JSON from get-profile-sticky message response: %s"
msgstr "不能从获取配置文件粘性消息响应中读取JSON：%s"

#: ../src/pavucontrol.cc:229
msgid "get-profile-sticky message response is not a JSON value"
msgstr "获取配置文件粘性消息响应不是JSON值"

#: ../src/pavucontrol.cc:249 ../src/cardwidget.cc:153 ../src/cardwidget.cc:181
#, c-format
msgid "pa_context_send_message_to_object() failed: %s"
msgstr "pa_context_send_message_to_object(): %s"

#: ../src/pavucontrol.cc:267
#, c-format
msgid "could not read JSON from list-handlers message response: %s"
msgstr "无法从列表处理程序消息响应中读取JSON：%s"

#: ../src/pavucontrol.cc:276
msgid "list-handlers message response is not a JSON array"
msgstr "列表处理程序消息响应不是JSON数组"

#: ../src/pavucontrol.cc:324
msgid "list-handlers message response could not be parsed correctly"
msgstr "无法正确分析列表处理程序消息响应"

#: ../src/pavucontrol.cc:358
msgid "Card callback failure"
msgstr "声卡回调失败"

#: ../src/pavucontrol.cc:386
msgid "Sink callback failure"
msgstr "Sink 回调失败"

#: ../src/pavucontrol.cc:410
msgid "Source callback failure"
msgstr "源回调失败"

#: ../src/pavucontrol.cc:429
msgid "Sink input callback failure"
msgstr "Sink 输入回调失败"

#: ../src/pavucontrol.cc:448
msgid "Source output callback failure"
msgstr "源输出回调失败"

#: ../src/pavucontrol.cc:478
msgid "Client callback failure"
msgstr "客户端回调失败"

#: ../src/pavucontrol.cc:494
msgid "Server info callback failure"
msgstr "服务器信息回调失败"

#: ../src/pavucontrol.cc:512 ../src/pavucontrol.cc:809
#, c-format
msgid "Failed to initialize stream_restore extension: %s"
msgstr "无法初始化 stream_restore 扩展：%s"

#: ../src/pavucontrol.cc:530
msgid "pa_ext_stream_restore_read() failed"
msgstr "pa_ext_stream_restore_read() 失败"

#: ../src/pavucontrol.cc:548 ../src/pavucontrol.cc:823
#, c-format
msgid "Failed to initialize device restore extension: %s"
msgstr "无法初始化设备恢复扩展：%s"

#: ../src/pavucontrol.cc:569
msgid "pa_ext_device_restore_read_sink_formats() failed"
msgstr "pa_ext_device_restore_read_sink_formats() 失败"

#: ../src/pavucontrol.cc:587 ../src/pavucontrol.cc:836
#, c-format
msgid "Failed to initialize device manager extension: %s"
msgstr "无法初始化设备管理扩展：%s"

#: ../src/pavucontrol.cc:606
msgid "pa_ext_device_manager_read() failed"
msgstr "pa_ext_device_manager_read() 失败"

#: ../src/pavucontrol.cc:623
msgid "pa_context_get_sink_info_by_index() failed"
msgstr "pa_context_get_sink_info_by_index() 失败"

#: ../src/pavucontrol.cc:636
msgid "pa_context_get_source_info_by_index() failed"
msgstr "pa_context_get_source_info_by_index() 失败"

#: ../src/pavucontrol.cc:649 ../src/pavucontrol.cc:662
msgid "pa_context_get_sink_input_info() failed"
msgstr "pa_context_get_sink_input_info() 失败"

#: ../src/pavucontrol.cc:675
msgid "pa_context_get_client_info() failed"
msgstr "pa_context_get_client_info() 失败"

#: ../src/pavucontrol.cc:685 ../src/pavucontrol.cc:750
msgid "pa_context_get_server_info() failed"
msgstr "pa_context_get_server_info() 失败"

#: ../src/pavucontrol.cc:698
msgid "pa_context_get_card_info_by_index() failed"
msgstr "pa_context_get_card_info_by_index() 失败"

#: ../src/pavucontrol.cc:741
msgid "pa_context_subscribe() failed"
msgstr "pa_context_subscribe() 失败"

#: ../src/pavucontrol.cc:757
msgid "pa_context_client_info_list() failed"
msgstr "pa_context_client_info_list() 失败"

#: ../src/pavucontrol.cc:764
msgid "pa_context_get_card_info_list() failed"
msgstr "pa_context_get_card_info_list() 失败"

#: ../src/pavucontrol.cc:771
msgid "pa_context_get_sink_info_list() failed"
msgstr "pa_context_get_sink_info_list() 失败"

#: ../src/pavucontrol.cc:778
msgid "pa_context_get_source_info_list() failed"
msgstr "pa_context_get_source_info_list() 失败"

#: ../src/pavucontrol.cc:785
msgid "pa_context_get_sink_input_info_list() failed"
msgstr "pa_context_get_sink_input_info_list() 失败"

#: ../src/pavucontrol.cc:792
msgid "pa_context_get_source_output_info_list() failed"
msgstr "pa_context_get_source_output_info_list() 失败"

#: ../src/pavucontrol.cc:851 ../src/pavucontrol.cc:902
msgid "Connection failed, attempting reconnect"
msgstr "连接失败，正尝试重连"

#: ../src/pavucontrol.cc:889
msgid ""
"Connection to PulseAudio failed. Automatic retry in 5s\n"
"\n"
"In this case this is likely because PULSE_SERVER in the Environment/X11 Root "
"Window Properties\n"
"or default-server in client.conf is misconfigured.\n"
"This situation can also arise when PulseAudio crashed and left stale details "
"in the X11 Root Window.\n"
"If this is the case, then PulseAudio should autospawn again, or if this is "
"not configured you should\n"
"run start-pulseaudio-x11 manually."
msgstr ""
"连接到PulseAudio失败。5秒内自动重试\n"
"\n"
"在这种情况下，这可能是因为环境/X11根窗口属性中的PULSE_服务器\n"
"或client.conf中的默认服务器配置错误。\n"
"当PulseAudio崩溃并在X11根窗口中留下过时的详细信息时，也会出现这种情况。\n"
"如果是这种情况，则PulseAudio应再次自动切换，或者如果未配置，则应\n"
"手动运行start-pulseaudio-x11。"

#: ../src/cardwidget.cc:126
msgid "pa_context_set_card_profile_by_index() failed"
msgstr "pa_context_set_card_profile_by_index() 失败"

#: ../src/channelwidget.cc:101
#, c-format
msgid "<small>%0.0f%% (%0.2f dB)</small>"
msgstr "<small>%0.0f%% (%0.2f dB)</small>"

#: ../src/channelwidget.cc:103
#, c-format
msgid "<small>%0.0f%% (-&#8734; dB)</small>"
msgstr "<small>%0.0f%% (-&#8734; dB)</small>"

#: ../src/channelwidget.cc:106
#, c-format
msgid "%0.0f%%"
msgstr "%0.0f%%"

#: ../src/channelwidget.cc:139
msgid "<small>Silence</small>"
msgstr "<small>静音</small>"

#: ../src/channelwidget.cc:139
msgid "<small>Min</small>"
msgstr "<small>最小</small>"

#: ../src/channelwidget.cc:141
msgid "<small>100% (0 dB)</small>"
msgstr "<small>100% (0 dB)</small>"

#: ../src/channelwidget.cc:145
msgid "<small><i>Base</i></small>"
msgstr "<small><i>基础</i></small>"

#: ../src/devicewidget.cc:59
msgid "Rename Device..."
msgstr "重命名设备……"

#: ../src/devicewidget.cc:163
msgid "pa_context_set_port_latency_offset() failed"
msgstr "pa_context_set_port_latency_offset() 失败"

#: ../src/devicewidget.cc:244
msgid "Sorry, but device renaming is not supported."
msgstr "抱歉，不支持设备重命名。"

#: ../src/devicewidget.cc:249
msgid ""
"You need to load module-device-manager in the PulseAudio server in order to "
"rename devices"
msgstr "要重命名设备，你需要在 PulseAudio 服务器中加载 module-device-manager"

#: ../src/devicewidget.cc:262
msgid "_Cancel"
msgstr "取消（_C）"

#: ../src/devicewidget.cc:263
msgid "_OK"
msgstr "确定(_O)"

#: ../src/devicewidget.cc:270
msgid "pa_ext_device_manager_write() failed"
msgstr "pa_ext_device_manager_write() 失败"

#: ../src/mainwindow.cc:171
#, c-format
msgid "Error reading config file %s: %s"
msgstr "读取配置文件 %s 时出错：%s"

#: ../src/mainwindow.cc:250
msgid "Error saving preferences"
msgstr "保存配置时出错"

#: ../src/mainwindow.cc:258
#, c-format
msgid "Error writing config file %s"
msgstr "写配置文件 %s 时出错"

#: ../src/mainwindow.cc:322
msgid " (plugged in)"
msgstr " （已插入）"

#: ../src/mainwindow.cc:326 ../src/mainwindow.cc:434
msgid " (unavailable)"
msgstr " （不可用）"

#: ../src/mainwindow.cc:328 ../src/mainwindow.cc:431
msgid " (unplugged)"
msgstr " （未插入）"

#: ../src/mainwindow.cc:633
msgid "Failed to read data from stream"
msgstr "无法从流媒体中读取数据"

#: ../src/mainwindow.cc:677
msgid "Peak detect"
msgstr "尖峰探测"

#: ../src/mainwindow.cc:678
msgid "Failed to create monitoring stream"
msgstr "无法常见监视流媒体"

#: ../src/mainwindow.cc:693
msgid "Failed to connect monitoring stream"
msgstr "无法连接到监视流媒体"

#: ../src/mainwindow.cc:830
msgid ""
"Ignoring sink-input due to it being designated as an event and thus handled "
"by the Event widget"
msgstr "忽略接收器输入，因为它被指定为事件，因此由事件小部件处理"

#: ../src/mainwindow.cc:1005
msgid "System Sounds"
msgstr "系统声音"

#: ../src/mainwindow.cc:1351
msgid "Establishing connection to PulseAudio. Please wait..."
msgstr "正在建立到 PulseAudio 的连接，请稍候……"

#: ../src/rolewidget.cc:72
msgid "pa_ext_stream_restore_write() failed"
msgstr "pa_ext_stream_restore_write() 失败"

#: ../src/sinkinputwidget.cc:35
msgid "on"
msgstr "开"

#: ../src/sinkinputwidget.cc:38
msgid "Terminate Playback"
msgstr "结束回放"

#: ../src/sinkinputwidget.cc:78
msgid "Unknown output"
msgstr "未知输出"

#: ../src/sinkinputwidget.cc:87
msgid "pa_context_set_sink_input_volume() failed"
msgstr "pa_context_set_sink_input_volume() 失败"

#: ../src/sinkinputwidget.cc:102
msgid "pa_context_set_sink_input_mute() failed"
msgstr "pa_context_set_sink_input_mute() 失败"

#: ../src/sinkinputwidget.cc:112
msgid "pa_context_kill_sink_input() failed"
msgstr "pa_context_kill_sink_input() 失败"

#: ../src/sinkwidget.cc:95
msgid "pa_context_set_sink_volume_by_index() failed"
msgstr "pa_context_set_sink_volume_by_index() 失败"

#: ../src/sinkwidget.cc:110
msgid "Volume Control Feedback Sound"
msgstr "音量控制反馈声音"

#: ../src/sinkwidget.cc:127
msgid "pa_context_set_sink_mute_by_index() failed"
msgstr "pa_context_set_sink_mute_by_index() 失败"

#: ../src/sinkwidget.cc:141
msgid "pa_context_set_default_sink() failed"
msgstr "pa_context_set_default_sink() 失败"

#: ../src/sinkwidget.cc:161
msgid "pa_context_set_sink_port_by_index() failed"
msgstr "pa_context_set_sink_port_by_index() 失败"

#: ../src/sinkwidget.cc:203
msgid "pa_ext_device_restore_save_sink_formats() failed"
msgstr "pa_ext_device_restore_read_sink_formats() 失败"

#: ../src/sourceoutputwidget.cc:35
msgid "from"
msgstr "来自"

#: ../src/sourceoutputwidget.cc:38
msgid "Terminate Recording"
msgstr "结束录音"

#: ../src/sourceoutputwidget.cc:83
msgid "Unknown input"
msgstr "未知输入"

#: ../src/sourceoutputwidget.cc:93
msgid "pa_context_set_source_output_volume() failed"
msgstr "pa_context_set_source_output_volume() 失败"

#: ../src/sourceoutputwidget.cc:108
msgid "pa_context_set_source_output_mute() failed"
msgstr "pa_context_set_source_output_mute() 失败"

#: ../src/sourceoutputwidget.cc:119
msgid "pa_context_kill_source_output() failed"
msgstr "pa_context_kill_source_output() 失败"

#: ../src/sourcewidget.cc:46
msgid "pa_context_set_source_volume_by_index() failed"
msgstr "pa_context_set_source_volume_by_index() 失败"

#: ../src/sourcewidget.cc:61
msgid "pa_context_set_source_mute_by_index() failed"
msgstr "pa_context_set_source_mute_by_index() 失败"

#: ../src/sourcewidget.cc:75
msgid "pa_context_set_default_source() failed"
msgstr "pa_context_set_default_source() 失败"

#: ../src/sourcewidget.cc:97
msgid "pa_context_set_source_port_by_index() failed"
msgstr "pa_context_set_source_port_by_index() 失败"

#: ../src/streamwidget.cc:52
msgid "Terminate"
msgstr "结束"

#: ../src/pavuapplication.cc:160
msgid "Select a specific tab on load."
msgstr "在加载时选取一个特定的表格。"

#: ../src/pavuapplication.cc:161
msgid "number"
msgstr "数字"

#: ../src/pavuapplication.cc:166
msgid "Retry forever if pa quits (every 5 seconds)."
msgstr "如果音频服务PulseAudio退出，则永远重试（每5秒一次）。"

#: ../src/pavuapplication.cc:171
msgid "Maximize the window."
msgstr "窗口最大化。"

#: ../src/pavuapplication.cc:176
msgid "Show version."
msgstr "显示版本号。"

#~ msgid "AC3"
#~ msgstr "AC3"

#~ msgid "EAC3"
#~ msgstr "EAC3"

#~ msgid "Device"
#~ msgstr "设备"

#~ msgid "pa_context_move_sink_input_by_index() failed"
#~ msgstr "pa_context_move_sink_input_by_index() 失败"

#~ msgid "pa_context_move_source_output_by_index() failed"
#~ msgstr "pa_context_move_source_output_by_index() 失败"
